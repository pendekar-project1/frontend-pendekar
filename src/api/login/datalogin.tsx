export type resultpengeluaran = {
    "username": string,
    "password": string
}


export async function datalogin(query: string) {
    if (query.trim() === "") return [];
    // /?q=${encodeURI(query)}
    
             
    const response = await fetch(
      `http://127.0.0.1:8090/login/query/`, {mode: 'no-cors'}
    );
    
    const results = await response.json();
    // console.log("response ", results)
    const documents = results as resultpengeluaran[];
    console.log(documents);
    return documents.slice(0, 10).map(({ username, password  }) => ({
        username, password
      }));
  }
