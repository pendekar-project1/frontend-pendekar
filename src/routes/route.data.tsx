import type { Component } from 'solid-js';
import {Routes, Route, Navigate} from '@solidjs/router';
import LandingPage from '../containers/landingPage/landingPage';
import Login from '../containers/login/login';
import Register from '../containers/register/register';
import Dashboard from '../containers/dashboard/dashboard';


const pathFn = (e: any) => {
  console.log('pathFn', e);
  return '/Landingpage';
}


const Root: Component = () => {
  return (
    <>
        <Routes>
            <Route path="/" element={ <Navigate href={pathFn}/> } />
            <Route path="/LandingPage" element={ <LandingPage/> } />
            <Route path="/login" element={ <Login/> } />
            <Route path="/register" element={ <Register/> } />
            <Route path="/dashboard" element={ <Dashboard/> } />
            {/* <Route path="/home" element={ <Home/> } /> */}
        </Routes>
    </>
  );
};


export default Root;