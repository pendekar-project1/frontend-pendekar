import { createEffect, type Component } from 'solid-js';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

/* Chart Component */
const PieChart1 : Component = () => {
  createEffect(() => {
    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    let chart = am4core.create("chartdiv", am4charts.PieChart);

    // Add data
    chart.data = [{
      "event": "Webinar",
      "count": 20
    }, {
      "event": "Volunteer",
      "count": 17
    }, {
      "event": "Lomba",
      "count": 12
    }];

    // Add and configure Series
    let pieSeries = chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = "count";
    pieSeries.dataFields.category = "event";
    pieSeries.slices.template.stroke = am4core.color("#fff");
    pieSeries.slices.template.strokeOpacity = 1;

    // This creates initial animation
    pieSeries.hiddenState.properties.opacity = 1;
    pieSeries.hiddenState.properties.endAngle = -90;
    pieSeries.hiddenState.properties.startAngle = -90;

    chart.hiddenState.properties.radius = am4core.percent(0);

    // Add title
    let title = chart.titles.create();
    title.text = "Event Summary";
    title.fontSize = 20;
    title.marginBottom = 20;
    title.marginTop = 20;

    // Cleanup when component unmounts
    return () => {
      chart.dispose();
    };
  }, []); // Empty dependency array ensures createEffect runs only once

  return <div id="chartdiv" style={{ width: "100%", height: "500px" }}></div>;
};

export default PieChart1;
