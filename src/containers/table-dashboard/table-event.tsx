// table_income_report.tsx
import type { Component } from 'solid-js';
import AgGridSolid from 'ag-grid-solid';
// import AgGridSolid from 'ag-grid-solid';
import 'ag-grid-community/styles/ag-grid.css';
import 'ag-grid-community/styles/ag-theme-alpine.css';
import { createSignal } from 'solid-js';

const Table_Event: Component = () => {
  const [gridApi, setGridApi] = createSignal(null);
  const columnDefs = [
    { field: 'bidang_event', headerName: 'Bidang Event' },
    { field: 'jumlah_pendaftaran', headerName: 'Jumlah Pendaftaran' },
  ];

  const defaultColDef = {
    flex: 1,
    filter: 'agTextColumnFilter',
  };

  const gridOptions = {
    rowHeight: 40,
    suppressCellSelection: true,
    getRowStyle: () => {
      return { paddingBottom: '10px' };
    },
  };

  const onGridReady = (params: { api: any; }) => {
    setGridApi(params.api);
  };

  // Simulasi data dari tabel events
  const eventsData = [
    // { bidang_event: 'event', poster_event: 'poster_url', deskripsi_event: 'Deskripsi event 1' },
    { bidang_event: 'webinar', deskripsi_event: 'Deskripsi webinar 1' },
    { bidang_event: 'volunteer', deskripsi_event: 'Deskripsi volunteer 1' },
    { bidang_event: 'lomba', deskripsi_event: 'Deskripsi lomba 1' },
    // Tambahkan lebih banyak data jika diperlukan
  ];

  // Simulasi data dari tabel registration_counts
  const registrationCountsData = [
    // { jumlah_pendaftaran: 10 },
    { jumlah_pendaftaran: 20 },
    { jumlah_pendaftaran: 17 },
    { jumlah_pendaftaran: 12 },
    // Tambahkan lebih banyak data jika diperlukan
  ];

  // Gabungkan data dari kedua tabel
  const mergedData = eventsData.map((event, index) => ({
    bidang_event: event.bidang_event,
    jumlah_pendaftaran: registrationCountsData[index]?.jumlah_pendaftaran || 0,
  }));

  return (
    <div>
      <div class="ag-theme-alpine" style={{ width: '28vw' }}>
        <AgGridSolid
          columnDefs={columnDefs}
          rowData={mergedData}
          defaultColDef={defaultColDef}
          domLayout='autoHeight'
          gridOptions={gridOptions}
          onGridReady={onGridReady}
        />
      </div>
    </div>
  );
};

export default Table_Event;
